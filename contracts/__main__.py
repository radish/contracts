import glob
import json
import os
from random import shuffle
from typing import List, Tuple, Iterator, Dict

import nltk
import numpy as np
from gensim.models import KeyedVectors
from sklearn.model_selection import train_test_split
from tensorflow.contrib.keras.python.keras import metrics, losses
from tensorflow.contrib.keras.python.keras.callbacks import ModelCheckpoint
from tensorflow.contrib.keras.python.keras.layers import Conv1D, MaxPool1D, Flatten, Concatenate, Dense, Input, Dropout
from tensorflow.contrib.keras.python.keras.models import Model


def prepare_model() -> Model:
    model_input = Input(shape=(768, 300))

    drop = Dropout(0.5)(model_input)

    conv_blocks = []
    for filter_size in (3, 8):
        conv = Conv1D(filters=10,
                      kernel_size=filter_size,
                      padding='valid',
                      activation='relu',
                      strides=1)(drop)
        conv = MaxPool1D(pool_size=2)(conv)
        conv = Flatten()(conv)
        conv_blocks.append(conv)

    x = Concatenate()(conv_blocks) if len(conv_blocks) > 1 else conv_blocks[0]
    x = Dropout(0.8)(x)
    x = Dense(80, activation='relu')(x)
    model_output = Dense(1, activation='sigmoid')(x)

    model = Model(model_input, model_output)
    model.compile(loss=losses.binary_crossentropy, optimizer='adam', metrics=[metrics.binary_accuracy])

    model.summary()

    return model


def transform_to_vectors(text: str, output_value: float, line_number: int,
                         vectors: KeyedVectors) -> Tuple[np.ndarray, np.ndarray]:
    words_vectors = []
    tokens = nltk.word_tokenize(text)
    for token in tokens:
        if token.isalpha():
            try:
                words_vectors.append(vectors.wv[token.lower()])
            except KeyError:
                pass

    words_vectors_matrix = np.zeros((768, 300), dtype=np.float32)
    words_vectors_matrix[0] = line_number
    for i, vector in enumerate(words_vectors):
        words_vectors_matrix[i + 1] = vector

    return words_vectors_matrix, np.array([output_value], dtype=np.float32)


def generator(files_paths: List[str], class_number: int,
              files_samples_count: Dict[str, int],
              w2v: KeyedVectors,
              batch_size: int) -> Iterator[Tuple[np.ndarray, np.ndarray]]:
    current_input_batch = np.ndarray((batch_size, 768, 300), dtype=np.float32)
    current_output_batch = np.ndarray((batch_size, 1), dtype=np.float32)
    current_batch_position = 0
    while True:
        shuffle(files_paths)

        for file_path in files_paths:
            with open(file_path) as json_file:
                enumerated_data = list(enumerate(json.load(json_file)))

            shuffle(enumerated_data)

            max_samples_count = files_samples_count[file_path]
            positive_samples_count = 0
            negative_samples_count = 0
            for i, line in enumerated_data:
                if current_batch_position == batch_size:
                    yield current_input_batch, current_output_batch
                    current_batch_position = 0

                if line['Class'] == class_number and positive_samples_count < max_samples_count:
                    network_input, network_output = transform_to_vectors(line['Text'], 1.0, i, w2v)
                    current_input_batch[current_batch_position] = network_input
                    current_output_batch[current_batch_position] = network_output
                    current_batch_position += 1
                    positive_samples_count += 1
                elif line['Class'] != class_number and negative_samples_count < max_samples_count:
                    network_input, network_output = transform_to_vectors(line['Text'], 0.0, i, w2v)
                    current_input_batch[current_batch_position] = network_input
                    current_output_batch[current_batch_position] = network_output
                    current_batch_position += 1
                    negative_samples_count += 1
                elif positive_samples_count >= max_samples_count and negative_samples_count >= max_samples_count:
                    break


def prepare_generator(files_paths: List[str], class_number: int,
                      w2v: KeyedVectors, batch_size: int=32) -> Tuple[Iterator[Tuple[np.ndarray, np.ndarray]], int]:
    total_count = 0

    files_samples_count = {}
    for file_path in files_paths:
        with open(file_path) as json_file:
            data = json.load(json_file)

        positive_count = 0
        negative_count = 0
        for line in data:
            if line['Class'] == class_number:
                positive_count += 1
            else:
                negative_count += 1

        samples_count = min((positive_count, negative_count))
        total_count += samples_count
        files_samples_count[file_path] = samples_count

    return generator(files_paths, class_number, files_samples_count, w2v, batch_size), total_count * 2 // batch_size


def main(class_number: int):
    directory_path = '/home/rivi/Contracts/marked'
    files_paths = glob.glob(os.path.join(directory_path, '*.json'))

    training_paths, validation_paths = train_test_split(files_paths, test_size=0.2)

    model = prepare_model()

    w2v: KeyedVectors = KeyedVectors.load_word2vec_format('/home/rivi/GoogleNews-vectors-negative300.bin', binary=True)
    training_generator, training_data_length = prepare_generator(training_paths, class_number, w2v)
    validation_generator, validation_data_length = prepare_generator(validation_paths, class_number, w2v)

    print(f'Training size: {training_data_length}')
    print(f'Validation size: {validation_data_length}')

    checkpointer = ModelCheckpoint(filepath=f'/home/rivi/Contracts/model_{class_number}.h5', verbose=1,
                                   save_best_only=True)
    model.fit_generator(training_generator, training_data_length, epochs=30, validation_data=validation_generator,
                        validation_steps=validation_data_length, callbacks=[checkpointer])


def predict():
    model = prepare_model()
    model.load_weights('/home/rivi/Contracts/model.h5')

    w2v: KeyedVectors = KeyedVectors.load_word2vec_format('/home/rivi/GoogleNews-vectors-negative300.bin', binary=True)

    with open('/home/rivi/Contracts/marked/1.json') as file:
        j = json.load(file)
        for i, line in enumerate(j):
            print(line['Text'], line['Class'])
            vectors, _ = transform_to_vectors(line['Text'], 0.0, i, w2v)
            print(model.predict(vectors, batch_size=1))


if __name__ == '__main__':
    main(0)
